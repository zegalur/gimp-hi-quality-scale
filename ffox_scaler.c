/*
 * High quality scale for GIMP by Zegalur.
 * 
 * Installation. 
 *      Type this into the terminal:
 *      LIBS=-lm gimptool-2.0 --install ffox_scaler.c
 * 
 * Change log:
 *      [empty now]
 * 
 */

#include <libgimp/gimp.h>
#include <libgimp/gimpui.h>
#include <libgimpmath/gimpmath.h>

#include <math.h>
#include <stdlib.h>

typedef gdouble (*FilterFuncPtr)(gdouble);

typedef struct {
    FilterFuncPtr   func;
    gdouble         width; // filter width define how many pixels from center this filter needs
    char*           description;
} Filter;

// available filters
static gdouble eval_mitchell_filter_value(gdouble x);
static gdouble eval_lanczos_filter_value(gdouble x);
static gdouble eval_hamming_filter_value(gdouble x);
static gdouble eval_box_filter_value(gdouble x);
static gdouble eval_triangle_filter_value(gdouble x);

Filter filters[] = {
    { eval_mitchell_filter_value,       3.0,    "Mitchell-Netravali"    },
    { eval_lanczos_filter_value,        3.0,    "Lanczos3"              },
    { eval_hamming_filter_value,        1.0,    "Hamming"               },
    { eval_box_filter_value,            0.5,    "Box"                   },
    { eval_triangle_filter_value,       1.0,    "Triangle"              },
    { 0,                                0.0,    0} // ! last filter always with zero pointer function
};

static void query       (void);

static void run         (const gchar      *name,
                         gint              nparams,
                         const GimpParam  *param,
                         gint             *nreturn_vals,
                         GimpParam       **return_vals);

static void scale       (GimpDrawable     *drawable,
                         gint              image_id);

static gboolean scale_dialog (GimpDrawable     *drawable,
                               gint              image_id);

static gint compute_conv_matrix(
                        gint                old_size,
                        gint                new_size,
                        gdouble           **weight_tab,
                        gint              **indx_tab,
                        FilterFuncPtr       filter_func,
                        gdouble             filter_width);

static void compute_line(guchar           *in_line,
                          guchar           *out_line,
                          gint              old_size,
                          gint              new_size,
                          gint              matrix_width,
                          gint              channels,
                          gdouble          *wtab,
                          gint             *itab,
                          gboolean          use_linear_color_space);
    

static gdouble gamma_tab[256];

typedef struct {
  gint new_width;
  gint new_height;
  gint use_liner_color_space;
  gint last_filter_indx;
  gint keep_aspect;
  gdouble last_aspect;
} FFoxScalerVals;

/* Set up default values for options */
static FFoxScalerVals svals = {
  -1,   // default new width value  (new image width must be equal to the original image width)
  -1,   // default new height value (new imgae height must be equal to the original image height)
  1,    // use linear color space model
  1,    // last used filter index
  1,    // keep aspect ratio as in original image
  1
};

GimpPlugInInfo PLUG_IN_INFO = {
  NULL,
  NULL,
  query,
  run
};

MAIN()

static void query (void) {
  static GimpParamDef args[] = {
    {
        GIMP_PDB_INT32,
        "run-mode",
        "Run mode"
    },
    {
        GIMP_PDB_IMAGE,
        "image",
        "Input image"
    },
    {
        GIMP_PDB_DRAWABLE,
        "drawable",
        "Input drawable"
    },
    {
        GIMP_PDB_INT32,
        "width",
        "New image width"
    },
    {
        GIMP_PDB_INT32,
        "height",
        "New image height"
    },
    {
        GIMP_PDB_INT32,
        "use_linear",
        "Use linear color space (1 - use linear color space, 0 - use gamma color space)"
    },
    {
        GIMP_PDB_INT32,
        "filter",
        "Filter index (see filter dialog)"
    }
  };

  gimp_install_procedure (
    "plug-in-hqscaler",
    "High quality scaler",
    "High quality scaling for GIMP",
    "Zegalur",
    "EMPTY",
    "2015",
    "_High quality scaler...",
    "RGB*, GRAY*",
    GIMP_PLUGIN,
    G_N_ELEMENTS (args), 0,
    args, NULL);

  gimp_plugin_menu_register ("plug-in-hqscaler",
                             "<Image>/Filters/Misc");
}

static void run(const gchar      *name,
                gint              nparams,
                const GimpParam  *param,
                gint             *nreturn_vals,
                GimpParam       **return_vals) {
    static GimpParam  values[1];
    GimpPDBStatusType status = GIMP_PDB_SUCCESS;
    GimpRunMode       run_mode;
    GimpDrawable     *drawable;
    gint              image_id;

    /* Setting mandatory output values */
    *nreturn_vals = 1;
    *return_vals  = values;

    values[0].type = GIMP_PDB_STATUS;
    values[0].data.d_status = status;

    /* Getting run_mode - we won't display a dialog if 
     * we are in NONINTERACTIVE mode */
    run_mode = param[0].data.d_int32;

    /* Get current image */
    image_id = param[1].data.d_int32;
    
    /*  Get the specified drawable  */
    drawable = gimp_drawable_get (param[2].data.d_drawable);

    switch (run_mode) {
        case GIMP_RUN_INTERACTIVE:
            /* Get options last values if needed */
            gimp_get_data ("plug-in-hqscaler", &svals);
            
            /* Get image aspect ratio */
            svals.last_aspect = (gdouble)(gimp_image_width(image_id)) / (gdouble)(gimp_image_height(image_id));

            /* Display the dialog */
            if (! scale_dialog (drawable, image_id))
                return;
            break;

        case GIMP_RUN_NONINTERACTIVE:
            if (nparams != 7)
                status = GIMP_PDB_CALLING_ERROR;
            if (status == GIMP_PDB_SUCCESS) {
                svals.new_width  = param[3].data.d_int32;
                svals.new_height = param[4].data.d_int32;
                svals.use_liner_color_space = param[5].data.d_int32;
                svals.last_filter_indx = param[6].data.d_int32;
            }
            break;

        case GIMP_RUN_WITH_LAST_VALS:
            /*  Get options last values if needed  */
            gimp_get_data ("plug-in-hqscaler", &svals);
            break;

        default:
            break;
    }

    scale(drawable, image_id);

    gimp_displays_flush ();
    gimp_drawable_detach (drawable);

    /*  Finally, set options in the core  */
    if(run_mode == GIMP_RUN_INTERACTIVE)
        gimp_set_data ("plug-in-hqscaler", &svals, sizeof (FFoxScalerVals));

    return;
}

void scale(GimpDrawable* drawable,
            gint          image_id) {
    guchar       *row,    *col;
    guchar       *outrow, *outcol;
    gint          i, j, k, channels;
    gint          old_width, old_height, new_width, new_height;
    gint          img_copy, in_layer, out1_layer, out2_layer;
    GimpDrawable *img_drawable, *out1_drawable, *out2_drawable;
    GimpPixelRgn  rgn_in, rgn1_out, rgn2_out;
    gdouble      *wtab_hor, *wtab_vert;
    gint         *itab_hor, *itab_vert;
    gint          hor_conv_mat_width;
    gint          vert_conv_mat_width;
    
    // pre-calculate gamma values
    for(i=0; i<256; ++i)
        gamma_tab[i] = pow((gdouble)(i)/255.0, 1.0/2.2);
    
    // duplicate the image and merge all visible layers
    img_copy = gimp_image_duplicate(image_id);
    in_layer = gimp_image_merge_visible_layers(img_copy, GIMP_CLIP_TO_IMAGE);
    out1_layer = gimp_layer_copy(in_layer);
    out2_layer = gimp_layer_copy(in_layer);
    if(gimp_image_add_layer(img_copy, out1_layer, -1) == FALSE) {
        // TODO: Error here!
    }
    if(gimp_image_add_layer(img_copy, out2_layer, -1) == FALSE) {
        // TODO: Error here!
    }
    
    // get whole image drawable
    img_drawable = gimp_drawable_get(in_layer);
    channels = gimp_drawable_bpp (img_drawable->drawable_id);
    
    // now it's scale image to half of its size
    if(svals.new_width < 0) 
        svals.new_width = img_drawable->width;
    if(svals.new_height < 0) 
        svals.new_height = img_drawable->height;
    
    old_width  = img_drawable->width;
    old_height = img_drawable->height;
    new_width  = svals.new_width;
    new_height = svals.new_height;
    
    // scale temporary layers
    gimp_layer_scale(out1_layer, new_width, old_height, FALSE);
    gimp_layer_scale(out2_layer, new_width, new_height, FALSE);
    out1_drawable = gimp_drawable_get(out1_layer);
    out2_drawable = gimp_drawable_get(out2_layer);
    
    gimp_progress_init ("Hi-quality scale...");
    
    /* Allocate a big enough tile cache */
    gimp_tile_cache_ntiles (MAX(2 * (old_width / gimp_tile_width () + 1),
                                2 * (old_height / gimp_tile_width () + 1)));
    
    
    gimp_drawable_flush(img_drawable);  
    gimp_drawable_flush(out1_drawable);  
    gimp_drawable_flush(out2_drawable);  
    
    // make horizontal scale
    // initialize pixel regions
    gimp_pixel_rgn_init (&rgn_in,
                        img_drawable,
                        0, 0, old_width, old_height, 
                        FALSE, FALSE);
    gimp_pixel_rgn_init (&rgn1_out,
                        out1_drawable,
                        0, 0, new_width, old_height, 
                        TRUE, TRUE);
    hor_conv_mat_width  = compute_conv_matrix(old_width,  
                                              new_width,  
                                              &wtab_hor,  
                                              &itab_hor,  
                                              filters[svals.last_filter_indx].func,
                                              filters[svals.last_filter_indx].width);
    row     = g_new (guchar, channels * old_width);
    outrow  = g_new (guchar, channels * new_width);
    for (i = 0; i < old_height; i++) {
        gimp_pixel_rgn_get_row (&rgn_in, row, 0, i, old_width);
        compute_line(row, outrow, old_width, new_width, hor_conv_mat_width, channels, wtab_hor, itab_hor, svals.use_liner_color_space);
        gimp_pixel_rgn_set_row (&rgn1_out, outrow, 0, i, new_width);
        if (i % 10 == 0)
            gimp_progress_update (0.5 * (gdouble) (i) / (gdouble) (old_height));
    }
    gimp_drawable_flush(out1_drawable);
    gimp_drawable_merge_shadow (out1_drawable->drawable_id, FALSE);
    g_free (row);
    g_free (outrow);
    g_free (wtab_hor);
    g_free (itab_hor);
    
    // make vertical scale
    gimp_pixel_rgn_init (&rgn1_out,
                        out1_drawable,
                        0, 0, new_width, old_height, 
                        FALSE, FALSE);
    gimp_pixel_rgn_init (&rgn2_out,
                        out2_drawable,
                        0, 0, new_width, new_height, 
                        TRUE, TRUE);
    vert_conv_mat_width = compute_conv_matrix(old_height, 
                                              new_height, 
                                              &wtab_vert, 
                                              &itab_vert,
                                              filters[svals.last_filter_indx].func,
                                              filters[svals.last_filter_indx].width);
    col     = g_new (guchar, channels * old_height);
    outcol  = g_new (guchar, channels * new_height);
    for (i = 0; i < new_width; i++) {
        gimp_pixel_rgn_get_col (&rgn1_out, col, i, 0, old_height);
        compute_line(col, outcol, old_height, new_height, vert_conv_mat_width, channels, wtab_vert, itab_vert, svals.use_liner_color_space);
        gimp_pixel_rgn_set_col (&rgn2_out, outcol, i, 0, new_height);
        if (i % 10 == 0)
            gimp_progress_update (0.5 + 0.5 * (gdouble) (i) / (gdouble) (new_width));
    }
    gimp_drawable_flush(out2_drawable);
    gimp_drawable_merge_shadow (out2_drawable->drawable_id, FALSE);
    g_free (col);
    g_free (outcol);
    g_free (wtab_vert);
    g_free (itab_vert);
    
    gimp_drawable_flush(img_drawable);
    
    // create final image
    gint final_img_id = gimp_image_new(svals.new_width, svals.new_height, GIMP_RGB);
    gint layer_id = gimp_layer_new_from_drawable(out2_drawable->drawable_id, final_img_id);
    gboolean leyer_res = gimp_image_insert_layer(final_img_id, layer_id, 0, -1);
    gint fimgdisplay_id = gimp_display_new(final_img_id);
        
    // free memory
    gimp_drawable_detach(img_drawable);
    gimp_drawable_detach(out1_drawable);
    gimp_drawable_detach(out2_drawable);
    gimp_image_delete(img_copy);
}

// scale one row or column
static void compute_line(guchar           *in_line,
                          guchar           *out_line,
                          gint              old_size,
                          gint              new_size,
                          gint              matrix_width,
                          gint              channels,
                          gdouble          *wtab,
                          gint             *itab,
                          gboolean          use_linear_color_space) {
    gint i, j, k;
    gdouble sum, src_val, conv_weight;
    gint src_indx;
    guchar dest_val;
    
    for(i = 0; i < new_size; ++i) {
        for(j = 0; j < channels; ++j) {
            sum = 0;
            for(k = 0; k<matrix_width; ++k) {
                src_indx = itab[i*matrix_width + k];
                src_val = (gdouble)(in_line[channels * src_indx + j]) / 255.0;
                
                if(use_linear_color_space) {
                    // convert to linear color space
                    src_val = pow(src_val, 2.2);
                }
                
                sum += src_val * wtab[i*matrix_width + k];
            }
            sum = CLAMP(sum, 0.0, 1.0);
            
            if(use_linear_color_space) {
                // Gamma correction to non-linear (display) color space
                sum = pow(sum, 1.0/2.2);
            }
            
            dest_val = (guchar)(floor(sum * 255.0 + 0.5));
            dest_val = CLAMP(dest_val, 0, 255);
            out_line[channels * i + j] = dest_val;
        }
    }
}

static gdouble eval_mitchell_filter_value(gdouble x) {
    gdouble B = 1.0/3.0;
    gdouble C = 1.0/3.0;
    x = ABS(x);
    if (x > 2.0) {
        return 0.0;
    } else if (x > 1.0) {
        return ((-B - 6.0*C) * x*x*x + (6.0*B + 30.0*C) * x*x +
                (-12.0*B - 48.0*C) * x + (8.0*B + 24.0*C)) * (1.0/6.0);
    } else {
        return ((12.0 - 9.0*B - 6.0*C) * x*x*x +
                (-18.0 + 12.0*B + 6.0*C) * x*x +
                (6.0 - 2.0*B)) * (1.0/6.0);
    }
}

static gdouble eval_lanczos_filter_value(gdouble x) {
    gdouble width = 3.0;
    gdouble xpi = x * M_PI;
    
    if (x <= -width || x >= width) {
        return 0.0; // Outside of the window.
    }
    if (x > -FLT_EPSILON && x < FLT_EPSILON) {
        return 1.0; // Special case the discontinuity at the origin.
    }
    
    return (sin(xpi) / xpi) * // sinc(x)
            sin(xpi / width) / (xpi / width); // sinc(x/fWidth)
}

static gdouble eval_hamming_filter_value(gdouble x) {
    gdouble width = 1.0;
    
    if (x <= -width || x >= width) {
        return 0.0f; // Outside of the window.
    }
    if (x > -FLT_EPSILON && x < FLT_EPSILON) {
        return 1.0f; // Special case the sinc discontinuity at the origin.
    }
    
    const float xpi = x * (gdouble)(M_PI);
    return ((sin(xpi) / xpi) * // sinc(x)
            (0.54f + 0.46f * cos(xpi / width))); // hamming(x)
}

static gdouble eval_box_filter_value(gdouble x) {
    gdouble width = 0.5;
    return (x >= -width && x < width) ? 1.0f : 0.0f;
}

static gdouble eval_triangle_filter_value(gdouble x) {
    gdouble width = 1.0;
    return MAX(0.f, width - fabsf(x));
}

/*
 * Calculate a convolution matrix coefficients.
 * WARNING! You must manually free (*weight_tab) and (*indx_tab)!
 * 
 * INPUT:
 *      old_size - old row size in pixels
 *      new_size - new row size in pixels
 * 
 * OUTPUT:
 *      *weight_tab - a table of weights for the convolution.  (size is new_size x return_value)
 *      *indx_tab   - a table of indexes for the convolution.  (size is new_size x return_value)
 * 
 * RETURN:
 *      Return the matrices width.
 */
static gint compute_conv_matrix(
                        gint                old_size,
                        gint                new_size,
                        gdouble           **weight_tab,
                        gint              **indx_tab,
                        FilterFuncPtr       filter_func,
                        gdouble             filter_width) {
    gint    dest_i, src_i, i, j;
    gdouble src_pixel;
    gint    src_begin, src_end;
    gdouble src_filter_dist;
    gdouble dest_filter_dist;
    gdouble filter_sum;
    gdouble filter_value, inv_filter_sum;
    gdouble rsum;
    
    gdouble scale = (gdouble)(new_size) / (gdouble)(old_size);
    
    // Speed up the divisions below by turning them into multiplies.
    gdouble inv_scale = 1.0 / scale;
    
    // When we're doing a magnification, the scale will be larger than one. This
    // means the destination pixels are much smaller than the source pixels, and
    // that the range covered by the filter won't necessarily cover any source
    // pixel boundaries. Therefore, we use these clamped values (max of 1) for
    // some computations.
    gdouble clamped_scale = MIN(1.0, scale);
    
    // This is how many source pixels from the center we need to count
    // to support the filtering function. 
    gdouble src_support = filter_width / clamped_scale;
    
    guint   src_support_i = (guint)src_support + 2;
    
    // allocate arrays memory
    gint    mwidth = (src_support_i * 2 + 1);
    gdouble *wtab = g_new (gdouble, new_size * mwidth);
    gint    *itab = g_new (gint,    new_size * mwidth);
    
    for(dest_i = 0; dest_i < new_size; ++dest_i) {
        // This is the pixel in the source directly under the pixel in the dest.
        // Note that we base computations on the "center" of the pixels. To see
        // why, observe that the destination pixel at coordinates (0, 0) in a 5.0x
        // downscale should "cover" the pixels around the pixel with *its center*
        // at coordinates (2.5, 2.5) in the source, not those around (0, 0).
        // Hence we need to scale coordinates (0.5, 0.5), not (0, 0).
        src_pixel = ((gdouble)(dest_i) + 0.5) * inv_scale;
        
        // Compute the (inclusive) range of source pixels the filter covers.
        src_begin = MAX(0,            (gint)(src_pixel - src_support));             // floor
        src_end   = MIN((old_size-1), (gint)ceil(src_pixel + src_support));         // ceil
        
        // Compute the unnormalized filter value at each location of the source
        // it covers.
        filter_sum = 0.0; // Sub of the filter values for normalizing.
        
        for (src_i = src_begin, i = 0; (src_i <= src_end) && (i<mwidth); ++src_i, ++i) {
            // Distance from the center of the filter, this is the filter coordinate
            // in source space. We also need to consider the center of the pixel
            // when comparing distance against 'srcPixel'. In the 5x downscale
            // example used above the distance from the center of the filter to
            // the pixel with coordinates (2, 2) should be 0, because its center
            // is at (2.5, 2.5).
            src_filter_dist = ((gdouble)(src_i) + 0.5) - src_pixel;
            
            // Since the filter really exists in dest space, map it there.
            dest_filter_dist = src_filter_dist * clamped_scale;
            
            // Compute the filter value at that location.
            filter_value = filter_func(dest_filter_dist);
            
            wtab[mwidth*(dest_i) + i] = filter_value;
            itab[mwidth*(dest_i) + i] = src_i;
            
            filter_sum += filter_value;
        }
        j = i;
        for(; i < mwidth; ++i) {
            wtab[mwidth*(dest_i) + i] = 0;
            itab[mwidth*(dest_i) + i] = 0;
        }
        
        // normalize weights
        inv_filter_sum = 1.0 / filter_sum;//(filter_sum != 0 ? 1.0 / filter_sum : 1.0);
        rsum = 0;
        for(i = 0; i < mwidth; ++i)  {
            wtab[mwidth*(dest_i) + i] *= inv_filter_sum;
            rsum += wtab[mwidth*(dest_i) + i];
        }
        
        // The normalization may leave some rounding errors, which
        // we add back in to avoid affecting the brightness of the image. We
        // arbitrarily add this to the center of the filter array (this won't always
        // be the center of the filter function since it could get clipped on the
        // edges, but it doesn't matter enough to worry about that case).
        if(j % 2)
            wtab[mwidth*(dest_i) + j/2] += 1.0 - rsum;
        else {
            if(j != 0) {
                wtab[mwidth*(dest_i) + j/2]   += (1.0 - rsum) / 2.0;
                wtab[mwidth*(dest_i) + j/2-1] += (1.0 - rsum) / 2.0;
            }
        }
    }
    
    *weight_tab = wtab;
    *indx_tab = itab;
    return mwidth;
}





// UI //

static void toggle_int (GtkToggleButton *togglebutton,
                         gpointer         user_data) {
    gint* d = (gint*)(user_data);
    if(*d)
        *d = 0;
    else
        *d = 1;
}

static void filter_changed(GtkComboBox *combo,
                           gpointer     user_data) {
    gint* d = (gint*)(user_data);
    *d = gtk_combo_box_get_active(combo);
}

static void width_changed (GtkAdjustment *adjustment,
                           gpointer       user_data) {
    if(svals.keep_aspect == 0)
        return;
    gdouble w = gtk_adjustment_get_value (adjustment);
    gdouble h = w / svals.last_aspect;
    gtk_adjustment_set_value((GtkAdjustment*)user_data, round(h));
}

static void height_changed (GtkAdjustment *adjustment,
                           gpointer       user_data) {
    if(svals.keep_aspect == 0)
        return;
    gdouble h = gtk_adjustment_get_value (adjustment);
    gdouble w = h * svals.last_aspect;
    gtk_adjustment_set_value((GtkAdjustment*)user_data, round(w));
}

static gboolean scale_dialog (GimpDrawable     *drawable,
                               gint              image_id) {
    GtkWidget *dialog;
    GtkWidget *main_vbox;
    GtkWidget *width_hbox;
    GtkWidget *height_hbox;
    GtkWidget *frame_img_dim;
    GtkWidget *frame_img_dim_vbox;
    GtkWidget *frame_interpolation;
    GtkWidget *frame_interpolation_vbox;
    GtkWidget *width_label;
    GtkWidget *height_label;
    GtkWidget *orig_size_label;
    GtkWidget *width_aligment;
    GtkWidget *height_aligment;
    GtkWidget *spin_width;
    GtkObject *spin_width_adj;
    GtkWidget *spin_height;
    GtkObject *spin_height_adj;
    GtkWidget *frame_img_dim_label;
    GtkWidget *frame_interpolation_label;
    GtkWidget *use_lin_color;
    GtkWidget *keep_aspect_ration;
    GtkWidget *filter_type;
    gboolean   run;
    gdouble    width_val;
    gdouble    height_val;
    gint       i;
    gdouble    orig_aspect;
    gint       orig_img_width;
    gint       orig_img_height;
    gchar      img_size_text[256];

    gimp_ui_init ("hqscaler", FALSE);
    
    width_val = svals.new_width;
    height_val = svals.new_height;
    
    orig_img_width  = gimp_image_width(image_id);
    orig_img_height = gimp_image_height(image_id);
    
    if(svals.new_width < 0) 
        width_val = orig_img_width;
    if(svals.new_height < 0) 
        height_val = orig_img_height;
    
    orig_aspect = (gdouble)(orig_img_width) / (gdouble)(orig_img_height);
    svals.last_aspect = orig_aspect;
    
    dialog = gimp_dialog_new ("Hi-quality scaler by Zegalur", "hqscaler",
                              NULL, 0,
                              gimp_standard_help_func, "plug-in-hqscaler",
                              GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                              GTK_STOCK_OK,     GTK_RESPONSE_OK,
                              NULL);

    main_vbox = gtk_vbox_new (FALSE, 6);
    gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), main_vbox);
    gtk_widget_show (main_vbox);

    // Image size frame ///////////
    
    frame_img_dim = gtk_frame_new (NULL);
    gtk_widget_show (frame_img_dim);
    gtk_box_pack_start (GTK_BOX (main_vbox), frame_img_dim, TRUE, TRUE, 0);
    gtk_container_set_border_width (GTK_CONTAINER (frame_img_dim), 6);
    
    frame_img_dim_vbox = gtk_vbox_new (FALSE, 6);
    gtk_container_add (GTK_CONTAINER (frame_img_dim), frame_img_dim_vbox);
    gtk_widget_show (frame_img_dim_vbox);
    
    frame_img_dim_label = gtk_label_new ("<b>Setup destination image size</b>");
    gtk_widget_show (frame_img_dim_label);
    gtk_frame_set_label_widget (GTK_FRAME (frame_img_dim), frame_img_dim_label);
    gtk_label_set_use_markup (GTK_LABEL (frame_img_dim_label), TRUE);  

    sprintf(img_size_text, "Original image size: %d x %d", orig_img_width, orig_img_height);
    orig_size_label = gtk_label_new_with_mnemonic (img_size_text);
    gtk_widget_show (orig_size_label);
    gtk_box_pack_start (GTK_BOX (frame_img_dim_vbox), orig_size_label, FALSE, FALSE, 6);
    gtk_label_set_justify (GTK_LABEL (orig_size_label), GTK_JUSTIFY_RIGHT);
    
        // width setup 
    
        width_aligment = gtk_alignment_new (0.5, 0.5, 1, 1);
        gtk_widget_show (width_aligment);
        gtk_container_add (GTK_CONTAINER (frame_img_dim_vbox), width_aligment);
        gtk_alignment_set_padding (GTK_ALIGNMENT (width_aligment), 6, 6, 16, 6);
    
        width_hbox = gtk_hbox_new (FALSE, 0);
        gtk_widget_show (width_hbox);
        gtk_container_add (GTK_CONTAINER (width_aligment), width_hbox);

        width_label = gtk_label_new_with_mnemonic ("Width:");
        gtk_widget_show (width_label);
        gtk_box_pack_start (GTK_BOX (width_hbox), width_label, FALSE, FALSE, 6);
        gtk_label_set_justify (GTK_LABEL (width_label), GTK_JUSTIFY_RIGHT);

        spin_width_adj = gtk_adjustment_new (width_val, 1, 100000, 1, 5, 5);
        spin_width = gtk_spin_button_new (GTK_ADJUSTMENT (spin_width_adj), 1, 0);
        gtk_widget_show (spin_width);
        gtk_box_pack_start (GTK_BOX (width_hbox), spin_width, FALSE, FALSE, 6);
        gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (spin_width), TRUE);
    
    // height setup 
        
    height_aligment = gtk_alignment_new (0.5, 0.5, 1, 1);
    gtk_widget_show (height_aligment);
    gtk_container_add (GTK_CONTAINER (frame_img_dim_vbox), height_aligment);
    gtk_alignment_set_padding (GTK_ALIGNMENT (height_aligment), 6, 6, 16, 6);
        
    height_hbox = gtk_hbox_new (FALSE, 0);
    gtk_widget_show (height_hbox);
    gtk_container_add (GTK_CONTAINER (height_aligment), height_hbox);
    
    height_label = gtk_label_new_with_mnemonic ("Height:");
    gtk_widget_show (height_label);
    gtk_box_pack_start (GTK_BOX (height_hbox), height_label, FALSE, FALSE, 6);
    gtk_label_set_justify (GTK_LABEL (height_label), GTK_JUSTIFY_RIGHT);
    
    spin_height_adj = gtk_adjustment_new (height_val, 1, 100000, 1, 5, 5);
    spin_height = gtk_spin_button_new (GTK_ADJUSTMENT (spin_height_adj), 1, 0);
    gtk_widget_show (spin_height);
    gtk_box_pack_start (GTK_BOX (height_hbox), spin_height, FALSE, FALSE, 6);
    gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (spin_height), TRUE);
    
        // keep aspect ration 
        keep_aspect_ration = gtk_check_button_new_with_label("Keep original aspect ratio");
        gtk_toggle_button_set_active ((GTK_TOGGLE_BUTTON(keep_aspect_ration)), svals.keep_aspect);
        gtk_widget_show (keep_aspect_ration);
        gtk_box_pack_start (GTK_BOX (frame_img_dim_vbox), keep_aspect_ration, TRUE, TRUE, 0);

    
    // Interpolation settings frame ///////////
    
    frame_interpolation = gtk_frame_new (NULL);
    gtk_widget_show (frame_interpolation);
    gtk_box_pack_start (GTK_BOX (main_vbox), frame_interpolation, TRUE, TRUE, 0);
    gtk_container_set_border_width (GTK_CONTAINER (frame_interpolation), 6);
    
    frame_interpolation_vbox = gtk_vbox_new (FALSE, 6);
    gtk_container_add (GTK_CONTAINER (frame_interpolation), frame_interpolation_vbox);
    gtk_widget_show (frame_interpolation_vbox);
    
    frame_interpolation_label = gtk_label_new ("<b>Setup interpolation/filtering settings</b>");
    gtk_widget_show (frame_interpolation_label);
    gtk_frame_set_label_widget (GTK_FRAME (frame_interpolation), frame_interpolation_label);
    gtk_label_set_use_markup (GTK_LABEL (frame_interpolation_label), TRUE);  
    
    // filter selection
    filter_type = gtk_combo_box_new_text ();
    for(i = 0; filters[i].func; ++i) 
        gtk_combo_box_append_text((GtkComboBox*)filter_type, filters[i].description); 
    gtk_combo_box_set_active((GtkComboBox*)filter_type, svals.last_filter_indx);
    gtk_widget_show (filter_type);
    gtk_box_pack_start (GTK_BOX (frame_interpolation_vbox), filter_type, TRUE, TRUE, 0);
    
    // linear color space 
    use_lin_color = gtk_check_button_new_with_label("Use linear color space");
    gtk_toggle_button_set_active ((GTK_TOGGLE_BUTTON(use_lin_color)), svals.use_liner_color_space);
    gtk_widget_show (use_lin_color);
    gtk_box_pack_start (GTK_BOX (frame_interpolation_vbox), use_lin_color, TRUE, TRUE, 0);

        g_signal_connect (spin_width_adj,  "value_changed",
                          G_CALLBACK (gimp_int_adjustment_update),
                          &svals.new_width);
        g_signal_connect (spin_height_adj, "value_changed",
                          G_CALLBACK (gimp_int_adjustment_update),
                          &svals.new_height);
        g_signal_connect (spin_width_adj,  "value_changed",
                          G_CALLBACK (width_changed),
                          spin_height_adj);
        g_signal_connect (spin_height_adj, "value_changed",
                          G_CALLBACK (height_changed),
                          spin_width_adj);
        g_signal_connect (GTK_TOGGLE_BUTTON(use_lin_color), "toggled",
                          G_CALLBACK (toggle_int),
                          &svals.use_liner_color_space);
        g_signal_connect (GTK_TOGGLE_BUTTON(keep_aspect_ration), "toggled",
                          G_CALLBACK (toggle_int),
                          &svals.keep_aspect);
        g_signal_connect (GTK_TOGGLE_BUTTON(filter_type), "changed",
                          G_CALLBACK (filter_changed),
                          &svals.last_filter_indx);
        

        gtk_widget_show (dialog);

        run = (gimp_dialog_run (GIMP_DIALOG (dialog)) == GTK_RESPONSE_OK);

        gtk_widget_destroy (dialog);

        return run;
}